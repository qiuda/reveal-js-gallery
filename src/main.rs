#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;

extern crate rocket_contrib;
extern crate mylib;

use mylib::get_data;

use std::collections::HashMap;
use std::path::{Path, PathBuf};

use rocket::response::NamedFile;
use rocket::response::Redirect;
use rocket_contrib::templates::Template;
use rocket::request::{Form};

#[macro_use]
extern crate serde_derive;

extern crate serde_json;

#[derive(FromForm)]
struct Slide{
    path: String,
}

#[derive(Serialize)]
struct TemplateContext {
    title: &'static str,
    name: Option<String>,
    items: mylib::Obj,
    parent: &'static str,
}

#[get("/")]
fn index() -> Template {
    let p = get_data().unwrap();
    Template::render("index", &TemplateContext {
        title: "Hello",
        name: None,
        items: p,
        parent: "layout",
    })
}

#[get("/<file..>", rank=3)]
fn files(file: PathBuf) -> Option<NamedFile> {
    NamedFile::open(Path::new("static/").join(file)).ok()
}

#[post("/presentation", data = "<slide>")]
fn presentation(slide: Form<Slide>) -> Redirect {
    let path = slide.into_inner().path;
    Redirect::to(format!("/presentation/{}", path))
}

#[get("/presentation/<url>", rank=2)]
fn get_presentation(url: String) -> Template {
    let s = get_data().unwrap();
    let mut id: usize = 0;
    while id < s.slides.len() {
        let u = &s.slides[id].url.to_string();
        if u == url.trim() {
            break;
        }
        id += 1;
    }
    let s = &s.slides[id];
    let mut slide = HashMap::new();
    slide.insert("title", &s.title);
    slide.insert("description", &s.description);
    slide.insert("theme", &s.theme);
    slide.insert("file", &s.file);
    Template::render("presentation", &slide)
}

fn rocket() -> rocket::Rocket {
    rocket::ignite().mount("/", routes![index, presentation, get_presentation, files])
    .attach(Template::fairing())
}

fn main() {
    rocket().launch();
}

#[cfg(test)]
mod test {
    use super::rocket;
    use rocket::local::Client;
    use rocket::http::Status;
    //use rocket_contrib::templates::Template;

    #[test]
    fn test_hello() {
        let client = Client::new(rocket()).unwrap();
        let response = client.get("/").dispatch();
        assert_eq!(response.status(), Status::Ok);
        //assert_eq!(response.body_string(), Some(expected));
    }
}
